﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ScoreManager2 : ScoreManager
{
    public int HighScore2;
    public int Score2;
    public Text ScoreText2;

    void Start()
    {
        Score2 = Score + 0;
        if (Score2 >= 20)
        {
            ScoreText.text = "Score = "+ Score2;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Score2 > HighScore2)
        {
            HighScore2 = Score2;
            PlayerPrefs.SetInt("highScore", HighScore2);
        }
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            Score2++;
            ScoreText.text = "Score = " + Score2;
            Destroy(other.gameObject);
           

        }

    }

   
}


