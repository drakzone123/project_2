﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundmanager : MonoBehaviour
{
    public AudioSource Coinsound;
    public AudioSource Stucksound;
    void OnTriggerEnter(Collider Collect)
    {
        if(Collect.gameObject.tag=="Coin")
        {
            Coinsound.Play();
        }
        if(Collect.gameObject.tag== "Obstacle")
        {
            Stucksound.Play();
        }
    }
   
}