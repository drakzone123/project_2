﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameplayController : MonoBehaviour
{
    public static GameplayController instance;
    public Transform[] lanes;
    private controller  playerController;
    public float minDeday = 8f;
    public float maxDelay = 40f;
    public float halfLength;
    public GameObject[] obtaclesPrefab;
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        halfLength = 100f;
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<controller>();
        StartCoroutine("generateObstacle");
    }
        
    IEnumerator generateObstacle()
     {
        float timer = Random.Range(minDeday, maxDelay);
        yield return new WaitForSeconds(timer);
        createObstacles(playerController.transform.position.z + halfLength);
        StartCoroutine("generateObstacle");           
    }
    
    private void createObstacles(float zpos)
    {
        int obstacleLane = Random.Range(0, lanes.Length);
        int obstacleType = Random.Range(0, obtaclesPrefab.Length);
        AddObstacle(new Vector3(lanes[obstacleLane].transform.position.x, 0, zpos), obstacleType);    
    }
    public void AddObstacle(Vector3 position,int type)
    {
        GameObject obstacle = Instantiate(obtaclesPrefab[type], position, Quaternion.Euler(0f,180f,0f));
    }
}
